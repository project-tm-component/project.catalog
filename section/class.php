<?php

CBitrixComponent::includeComponentClass('project:component');

use Bitrix\Iblock,
    Bitrix\Main;

class projectCatalogSection extends projectComponent {

    protected function projectGetSelect() {
        return array_merge(parent::projectGetSelect(), array('IBLOCK_ID', 'DETAIL_PAGE_URL'));
    }

    protected function projectGetCacheKeys() {
        return array(
            'SECTION'
        );
    }

    public function executeComponent() {
        $this->projectFilterParam('iblock', 'section', 'count', 'filter', 'sort', 'meta');
        if ($this->projectCache()) {
            if (self::projectLoader('iblock')) {
                list($arSort, $arFilter) = $this->projectGetFilter('active', 'iblock', 'section', 'filter');
                $this->projectInitSection();
                $arSelect = $this->projectGetSelect();

                $arNavParams = $this->arParams["NEWS_COUNT"] ? array(
                    "nPageSize" => $this->arParams["NEWS_COUNT"]
                        ) : false;
                $res = CIBlockElement::GetList($arSort, $arFilter, false, $arNavParams, $arSelect);
                $res->SetUrlTemplates($this->arParams["DETAIL_URL"] ?: '', "", $this->arParams["IBLOCK_URL"] ?: '');
                $this->arResult['ITEMS'] = array();
                while ($arItem = $res->GetNext()) {
                    $arButtons = CIBlock::GetPanelButtons(
                                    $arItem["IBLOCK_ID"],
                                    $arItem["ID"],
                                    0,
                                    array("SECTION_BUTTONS" => false, "SESSID" => false)
                    );
                    $arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
                    $arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];
                    Bitrix\Iblock\Component\Tools::getFieldImageData(
                            $arItem, array('PREVIEW_PICTURE', 'DETAIL_PICTURE'), Bitrix\Iblock\Component\Tools::IPROPERTY_ENTITY_ELEMENT, 'IPROPERTY_VALUES'
                    );
                    $this->arResult['ITEMS'][] = $arItem;
                }
                $this->projectResultCacheKeys();
                $this->projectTemplate();
            }
        }
        if (!empty($this->arParams['META'])) {
            $this->initMetaData();
        }
        return $this->arResult;
    }

    protected function initMetaData() {
        global $APPLICATION;
        if (empty($this->arResult['SECTION']['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'])) {
            $APPLICATION->SetTitle($this->arResult['NAME']);
        } else {
            $APPLICATION->SetTitle($this->arResult['SECTION']['IPROPERTY_VALUES']['SECTION_PAGE_TITLE']);
        }

        if (empty($this->arResult['SECTION']['IPROPERTY_VALUES']['SECTION_META_TITLE'])) {
            $APPLICATION->SetPageProperty('title', $this->arResult['NAME']);
        } else {
            $APPLICATION->SetPageProperty('title', $this->arResult['SECTION']['IPROPERTY_VALUES']['SECTION_META_TITLE']);
        }

        if ($this->arResult['SECTION']['IPROPERTY_VALUES']['SECTION_META_KEYWORDS']) {
            $APPLICATION->SetPageProperty('keywords', $this->arResult['SECTION']['IPROPERTY_VALUES']['SECTION_META_KEYWORDS']);
        }

        if ($this->arResult['SECTION']['IPROPERTY_VALUES']['SECTION_META_DESCRIPTION']) {
            $APPLICATION->SetPageProperty('description', $this->arResult['SECTION']['IPROPERTY_VALUES']['SECTION_META_DESCRIPTION']);
        }

        if ($this->arResult['PATH']) {
            foreach ($this->arResult['PATH'] as $path) {
                if (empty($path['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'])) {
                    $APPLICATION->AddChainItem($path['NAME'], $path['~SECTION_PAGE_URL']);
                } else {
                    $APPLICATION->AddChainItem($path['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'], $path['~SECTION_PAGE_URL']);
                }
            }
        }
        $APPLICATION->AddChainItem($path['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'], $path['~SECTION_PAGE_URL']);

        if ($this->arResult['ITEMS_TIMESTAMP_X']) {
            Main\Context::getCurrent()->getResponse()->setLastModified($this->arResult['ITEMS_TIMESTAMP_X']);
        }
    }

}

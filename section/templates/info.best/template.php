<div class="best__left-block best__lf--js">
    <? foreach ($arResult['ITEMS'] as $key => $arItem) { ?>
        <?
        $key += 1;
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="<? if ($key == 1) { ?>active<? } ?> best__lf-item-js best__lf-item-mobile">
            <div class="best__list--img">
                <img class="it__<?= $key ?>" src="<?= $arItem['PROPERTY_ICON_VALUE'] ?>">
            </div>
            <div class="best__list--title"><?= $arItem['NAME'] ?></div>
            <div class="best__list--text"><?= $arItem['PROPERTY_TEXT_VALUE'] ?></div>
        </div>
    <? } ?>
</div>
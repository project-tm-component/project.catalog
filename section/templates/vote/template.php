<div class="ny_body_categories ny_categories">
    <div class="ny_categories__content">
        <div class="center">
            <div class="ny_categories__title"><?= $arResult['SECTION']['NAME'] ?></div>
        </div>
    </div>
    <div class="ny_categories__container">
        <? foreach ($arResult['ITEMS'] as $arItem) { ?>
            <div class="ny_categories__wrap_item <?= $arResult['SECTION']['CODE'] ?> <? if ($arItem['STATUS']) { ?>vote-disabled<? } ?>">
                <a <? if(empty($arItem['STATUS'])) { ?>href="<?= $arItem['DETAIL_PAGE_URL'] ?>"<? } ?> class="ny_categories__item">
                    <img class="ny_categories__base_img" src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" alt="<?= $arItem['NAME'] ?>">
                    <img class="ny_categories__hover_img" src="<?= $arItem['DETAIL_PICTURE']['SRC'] ?>" alt="<?= $arItem['NAME'] ?>">
                </a>
            </div>
        <? } ?>
    </div>
</div>


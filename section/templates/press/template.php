<ul>
    <? foreach ($arResult['ITEMS'] as $arItem) { ?>
        <li>
            <a href="<?= $arItem['FILE'] ?>">
                <? if ($arItem['DETAIL_PICTURE']) { ?>
                    <img src="<?=$arItem['DETAIL_PICTURE']['SRC'] ?>" alt="<?= htmlspecialchars($arItem['NAME']) ?>">
                <? } ?>
                <span><?= $arItem['NAME'] ?></span>
            </a>
        </li>
    <? } ?>
</ul>
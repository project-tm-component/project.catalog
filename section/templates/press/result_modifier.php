<?

foreach ($arResult['ITEMS'] as &$arItem) {
    $arItem['FILE'] = cFile::getPath($arItem['PROPERTY_FILE_VALUE']);
    if (Jerff\Coralina\Lang::isEn()) {
        if (!empty($arItem['PROPERTY_EN_NAME_VALUE'])) {
            $arItem['NAME'] = $arItem['PROPERTY_EN_NAME_VALUE'];
        }
    }
}
<section class="mainSlider cleaFix">
    <div class="slider">
        <? foreach ($arResult['ITEMS'] as $arItem) { ?>
            <div>
                <? if (!empty($arItem['PROPERTY_LINK_VALUE'])) { ?>
                    <a href="<?= $arItem['PROPERTY_LINK_VALUE'] ?>">
                    <? } ?>
                <img src="<?= $arItem['DETAIL_PICTURE']['SRC'] ?>" alt="<?= htmlspecialchars($arItem['NAME']) ?>" />
            <? if (!empty($arItem['PROPERTY_LINK_VALUE'])) { ?>
                </a>
            <? } ?>
                </div>
        <? } ?>
    </div>
</section>
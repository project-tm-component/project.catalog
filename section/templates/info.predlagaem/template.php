<div class="services__items-block">
    <? foreach ($arResult['ITEMS'] as $arItem) { ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="services__item">
            <div class="card-container">
                <div class="card">
                    <div class="front">
                        <img src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" alt="<?= $arItem['NAME'] ?>">
                    </div>
                    <img src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" alt="<?= $arItem['NAME'] ?>">
                </div>
            </div>
            <p class="services__title"><?= $arItem['NAME'] ?></p>
            <p class="services__text"><?= $arItem['PROPERTY_TEXT_VALUE'] ?></p>
        </div>
    <? } ?>
</div>
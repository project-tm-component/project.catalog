<?

foreach ($arResult['ITEMS'] as &$arItem) {
    if ($arItem['DETAIL_PICTURE'] or $arItem['PREVIEW_PICTURE']) {
        $arItem['IMAGE'] = Project\Ver276ebf5263b8384672761a9eef2eef22432e2e68\Tools\Utility\Image::resize($arItem['PREVIEW_PICTURE']['ID'] ?? $arItem['DETAIL_PICTURE']['ID'], 353, 190);
    }
    if (Jerff\Coralina\Lang::isEn()) {
        if (!empty($arItem['PROPERTY_EN_NAME_VALUE'])) {
            $arItem['NAME'] = $arItem['PROPERTY_EN_NAME_VALUE'];
        }
        if (!empty($arItem['PROPERTY_EN_ANONS_VALUE'])) {
            $arItem['PREVIEW_TEXT'] = $arItem['PROPERTY_EN_ANONS_VALUE']['TEXT'];
        }
    }
}
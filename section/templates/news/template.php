<section class="newsList whiteBg">
    <div class="wrapper cleaFix">
        <div class="headLine withLink cleaFix">
            <?= Jerff\Coralina\Lang::include('main/news-start') ?>
        </div><!-- /headLine -->
        <ul class="cleaFix">
            <? foreach ($arResult['ITEMS'] as $arItem) { ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <li><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                        <? if (!empty($arItem['IMAGE'])) { ?>
                            <div class="newsImg"><img src="<?= $arItem['IMAGE'] ?>" alt="<?= htmlspecialchars($arItem['NAME']) ?>" /></div>
                        <? } ?>
                        <h4 class="newsHeadline"><?= $arItem['NAME'] ?></h4>
                        <p><?= $arItem['PREVIEW_TEXT'] ?></p>
                    </a>
                </li>
            <? } ?>
        </ul>
        <div class="functionalFooter"><a href="<?=SITE_DIR ?>about/news/"><span class="loadMore"><?= Jerff\Coralina\Lang::include('main/news-stop') ?></span></a></div>
    </div><!-- /wrapper -->
</section><!-- /newsList -->
<div class="best__wrap-js">
    <? foreach ($arResult['ITEMS'] as $key => $arItem) { ?>
        <?
        $key += 1;
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="<? if ($key == 1) { ?>active<? } ?> best__rg-list best__rg-list-js">
            <div class="best__list--img">
                <img class="it__<?= $key ?>" src="<?= $arItem['PROPERTY_ICON_VALUE'] ?>">
            </div>
            <div class="best__list--title"><?= $arItem['NAME'] ?></div>
        </div>
    <? } ?>
</div>
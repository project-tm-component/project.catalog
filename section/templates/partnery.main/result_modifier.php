<?

foreach ($arResult['ITEMS'] as $key=>&$arItem) {
    $arItem['IMAGE'] = cFile::getPath($arItem['PROPERTY_IMAGE_VALUE']);
    if (Jerff\Coralina\Lang::isEn()) {
        if (!empty($arItem['PROPERTY_EN_NAME_VALUE'])) {
            $arItem['NAME'] = $arItem['PROPERTY_EN_NAME_VALUE'];
        }
    }
}
$arResult['ITEMS'] = array_chunk($arResult['ITEMS'], 6);
<div class="patrnerLogos">
    <? foreach ($arResult['ITEMS'] as $list) { ?>
        <div class="row">
            <? foreach ($list as $arItem) { ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <span><img src="<?= $arItem['IMAGE'] ?>" alt="<?= htmlspecialchars($arItem['NAME']) ?>" /></span>
            <? } ?>
        </div>
    <? } ?>
</div>
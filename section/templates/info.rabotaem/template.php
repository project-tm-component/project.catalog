<div class="space__star_track container">
    <? foreach ($arResult['ITEMS'] as $key => $arItem) { ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="space__star<?= $key + 1 ?> space__star--all post hidden">
            <div class="space__star-img"></div>
            <div class="space__wrap">
                <div class="space__title"><?= $arItem['NAME'] ?></div>
                <div class="space__text"><?= $arItem['PROPERTY_TEXT_VALUE'] ?></div>
            </div>
        </div>
    <? } ?>
</div>
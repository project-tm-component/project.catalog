<?php

CBitrixComponent::includeComponentClass("project:component");

use Bitrix\Iblock;

class projectCatalogElement extends projectComponent {

    protected function projectSetCacheKeys() {
        return array(
            'ID',
            'NAME',
            'ITEMS_TIMESTAMP_X',
            'IPROPERTY_VALUES',
            'SECTION'
        );
    }

    public function executeComponent() {
        $this->projectFilterParam('iblock', 'section', 'element');
        if ($this->projectCache()) {
            if (self::projectLoader('iblock')) {
                list(, $arFilter) = $this->projectGetFilter('active', 'iblock', 'section', 'element');
                if ($arFilter['ID']) {
                    $arSelect = $this->projectGetSelect();
                    $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
                    if ($this->arResult = $res->GetNext()) {
                        $this->projectInitSection();
                        $ipropValues = new Iblock\InheritedProperty\ElementValues($arFilter['IBLOCK_ID'], $arFilter['ELEMENT_ID']);
                        $this->arResult['IPROPERTY_VALUES'] = $ipropValues->getValues();
                        Iblock\Component\Tools::getFieldImageData(
                                $this->arResult, array('PREVIEW_PICTURE', 'DETAIL_PICTURE'), Iblock\Component\Tools::IPROPERTY_ENTITY_ELEMENT, 'IPROPERTY_VALUES'
                        );
                        $this->projectResultCacheKeys();
                        $this->projectTemplate();
                    }
                }
            }
        }
        return $this->arResult['ID'] ?: 0;
    }

}

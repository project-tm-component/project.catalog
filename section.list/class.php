<?php

CBitrixComponent::includeComponentClass('project:component');

use Bitrix\Iblock;

class projectCatalogSectionList extends projectComponent {

    protected function projectGetSort() {
        return array(
            'LEFT_MARGIN' => 'ASC',
            $this->arParams['SORT_BY1'] => $this->arParams['SORT_ORDER1'],
            $this->arParams['SORT_BY2'] => $this->arParams['SORT_ORDER2'],
        );
    }

    protected function projectGetSelect() {
        $arSelect = array('ID', 'NAME', 'PICTURE', 'SECTION_PAGE_URL');
        if (!empty($this->arParams['SECTION_USER_FIELDS'])) {
            foreach ($this->arParams['SECTION_USER_FIELDS'] as $value) {
                if ($value) {
                    $arSelect[] = $value;
                }
            }
        }
        return array_unique($arSelect);
    }

    protected function projectGetCacheKeys() {
        return array('COUNT');
    }

    public function executeComponent() {
        $this->projectFilterParam('iblock', 'section', 'filter', 'sort');
        if ($this->projectCache()) {
            if (self::projectLoader('iblock')) {
                list($arSort, $arFilter) = $this->projectGetFilter('active', 'iblock', 'section', 'filter');
                $arSelect = $this->projectGetSelect();
                $boolPicture = in_array('PICTURE', $arSelect);
                $res = CIBlockSection::GetList($arSort, $arFilter, array('CNT_ACTIVE'), $arSelect);
                $res->SetUrlTemplates("", $this->arParams["SECTION_URL"] ?: '');
                $key = $this->projectGetKeyID();
                $this->arResult['SECTIONS'] = array();
                while ($arItem = $res->GetNext()) {
                    if ($boolPicture) {
                        Iblock\Component\Tools::getFieldImageData(
                                $arItem, array('PICTURE'), Iblock\Component\Tools::IPROPERTY_ENTITY_SECTION, 'IPROPERTY_VALUES'
                        );
                    }
                    $this->arResult['SECTIONS'][$arItem[$key]] = $arItem;
                }
                $this->projectResultSort($this->arResult['SECTIONS']);
                if ($this->arResult['SECTIONS']) {
                    $this->arResult['COUNT'] = count($this->arResult['SECTIONS']);
                    $this->projectResultCacheKeys();
                }
                $this->projectTemplate();
            }
        }
        return $this->arResult['COUNT'] ?: 0;
    }

}

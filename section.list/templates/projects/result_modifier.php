<?

$page = $APPLICATION->GetCurPage(false);
foreach ($arResult['SECTIONS'] as &$arSection) {
    if (Jerff\Coralina\Lang::isEn()) {
        if (!empty($arSection['UF_EN_NAME'])) {
            $arSection['NAME'] = $arSection['UF_EN_NAME'];
        }
    }
    $arSection['ACTIVE'] = $page == $arSection['SECTION_PAGE_URL'];
    if(empty($arSection['ACTIVE'])) {
        $arSection['ACTIVE'] = $arParams['SECTION_CODE_TEM'] == $arSection['CODE'];
    }
}
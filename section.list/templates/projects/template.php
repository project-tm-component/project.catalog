<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<select>
    <? foreach ($arResult['SECTIONS'] as &$arSection) { ?>
        <option value="<?= $arSection['SECTION_PAGE_URL'] ?>" <? if ($arSection['ACTIVE']) { ?>selected="selected"<? } ?>>�������<?= $arSection['NAME'] ?></option>
    <? } ?>
</select>
<nav class="sideNav">
    <? foreach ($arResult['SECTIONS'] as &$arSection) { ?>
        <a href="<?= $arSection['SECTION_PAGE_URL'] ?>" <? if ($arSection['ACTIVE']) { ?>class="active"<? } ?>><?= $arSection['NAME'] ?></a>
    <? } ?>
</nav><!-- /sideNav -->

<?
if (empty($arResult['SECTIONS'])) {
    return;
}
?>
<div class="projects__wrap">
    <? foreach ($arResult['SECTIONS'] as $key => $arSection) { ?>
        <? if (in_array($key, array(1, 2, 4))) { ?>
            <div class="projects__col-4">
            <? } ?>
            <a href="<?= $arSection['SECTION_PAGE_URL']; ?>" class="projects__item--<?= $key ?>"  style="background-image: url(<? echo $arSection['PICTURE']['SRC']; ?>);">
                <div class="projects__content">
                    <div class="projects__title"><?= $arSection['NAME'] ?></div>
                    <div class="projects__text"><?= $arSection['UF_TEXT'] ?></div>
                </div>
            </a>
            <? if (in_array($key, array(1, 3, 5))) { ?>
            </div>
        <? } ?>
    <? } ?>
</div>
<div class="center">
    <div class="orange_bth last__btn">Посмотреть все наши работы</div>
    <div class="orange_bth vis__btn">Посмотреть все работы</div>
</div>
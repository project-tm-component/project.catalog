<?
if (empty($arResult['SECTION'])) {
    return;
}
?>
<div class="categories _content">
    <div class="categories__inner">
        <? foreach ($arResult['SECTION'] as $arItem) { ?>
            <a class="categories__item _border" href="<?= $arItem['SECTION_PAGE_URL'] ?>">
                <figure>
                    <div class="categories__img"><img src="<?= $arItem['PICTURE'] ?>" alt=""/></div>
                    <figcaption class="categories__caption"><?= $arItem['NAME'] ?></figcaption>
                </figure>
            </a>
        <? } ?>
    </div>
</div>
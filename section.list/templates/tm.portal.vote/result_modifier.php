<?

$this->addExternalCss('/local/templates/markup/dist/ihover/src/ihover.min.css');

foreach ($arResult['SECTION'] as &$arItem) {
    if (empty($arItem['PICTURE'])) {
        $arItem['PICTURE'] = array(
            'SRC' => '', $this->GetFolder() . '/images/line-empty.png'
        );
    } else if (defined('Project\Tools\IS_START')) {
        $arItem['PICTURE']['SRC'] = Project\Tools\Utility\Image::resize($arItem['PICTURE']['ID'], 100, 100, BX_RESIZE_IMAGE_EXACT);
    }
}

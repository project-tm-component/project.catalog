<?
if (empty($arResult['SECTIONS'])) {
    return;
}
?>
<div class="vote-wrapper">
    <h1>Результаты голосования</h1>
    <? foreach ($arResult['SECTIONS'] as $arSection) { ?>
        <div class="vote-list">
            <!-- colored -->
            <div class="vote-item">
                <img src="<? echo $arSection['PICTURE']['SRC']; ?>" alt="<? echo $arSection['PICTURE']['TITLE']; ?>">
                <a href="<? echo $arSection['SECTION_PAGE_URL']; ?>">
                    <h3><? echo $arSection['NAME']; ?></h3>
                </a>
                <p><?= $arSection['DESCRIPTION']; ?></p>
                <a href="/local/modules/project.vote/tools/export.php?ID=<?= $arSection['ID'] ?>">Результаты</a>
            </div>
            <div class="clear"></div>
            <!-- end colored -->
        </div>
    <? } ?>
</div>